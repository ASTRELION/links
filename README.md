# Links

Forked from [LittleLink](https://github.com/sethcottle/littlelink) and customized for my own purposes. Also includes some [Font Awesome](https://fontawesome.com/download) icons.

View here: https://astrelion.gitlab.io/links